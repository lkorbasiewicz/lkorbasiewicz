## Łukasz Korbasiewicz's README

**Łukasz Korbasiewicz - Senior Support Engineer, GitLab** (he/his)

### Personally

- I'm originally from Poland, currently living in a tiny village on the outskirts of Prague, Czech Republic
- I love **coffee**. Feel free to schedule a [**Coffee** Chat](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) with me. This way I can have one more :)
- My wife makes the best **Tiramisu**... and other cakes. They taste even better with a good **coffee**.
- **I talk** a lot. Probably **too much**. If you notice that, feel free to ~~let me know~~ interrupt me. 
- It's probably related to my ADHD.
- If you just want to have someone endlessly talking in the background but you're tired by TV delivering only bad news - ask me about my son's piano skills, my wife's cakes or one of my hobbies.
- Some of my hobbies are: 
  - collecting musical instruments - I have 5 guitars, [a digital piano](https://www.kawai-global.com/product/kdp120/), [stage piano](https://www.kawai-global.com/product/es520/), [electronic drum kit](https://www.roland.com/cz/products/td-1dmk/), few harmonicas and my voice (this one is always out of tune though)
  - sometimes even playing them
  - drinking **coffee**
  - photography (photographing mostly, but not limited to, people)
  - gardening
  - woodworking
  - reading
  - hiking
  - fishing
  - eating (don't judge me, it's a valid hobby)
  - LOOK! A SQUIRREL!!

### Professionally

- I started at GitLab in July 2020
- Prior to that I had almost zero exposure to DevOps or Software Engineering (except learning python to automate some boring tasks)
- I was, however, a Support Engineer for most of my professional career, usually working with computer networks and network security
- I am trying to learn everything, however I focus on following areas:
  - AI (Duo)
  - Geo
  - CI/CD
  - API
  - ADHD

### What I assume about others

- You are good at what you're doing (otherwise you won't be here).
- [You mean well](https://about.gitlab.com/handbook/values/#assume-positive-intent)

### How to communicate with me

- I usually work between 9am - 6pm CE(S)T, however I do have non-linear workdays every now and then, so you can find me working at weird times. 2am here is 10am somewhere, right?
- I'm _always_ up for a **coffee** chat.
- I read email once a day. I don't check Slack too often as well, unless you `@mention` me. Feel free to do so if you want to grab my attention and make sure I read the message.
- Ask me anything. If I can help, I will do my best. If I cannot, I may know who can. 
- Did I mention that **I** tend to **talk too much**? 
- I can also sound like I know what I'm talking about even if I don't. Feel free to prove me wrong. I value learning more than being right.
- If I say or do something wrong, please let me know. Cultural differences or ignorance can cause problems that are easy to avoid if communicated early enough.

### How I like to give feedback

Constructive feedback - directly.
Positive feedback - publicly (Slack, Issues, MR's, Team Call docs etc.)

### How I like to receive feedback

Yes.

### How to contact me

You can find some information about my professional career on [LinkedIn](https://www.linkedin.com/in/korbasiewicz/)

If you're not a GitLab Team Member, you can [email me](mailto:lukasz.korbasiewicz@gmail.com)

GitLab Team Members can also find me on Slack, either via DM (@lkorbasiewicz) or in the following channels (amongst many others):

- #all-caps (DON'T YOU THINK IT SHOULD BE WRITTEN IN ALL CAPS?)
- #support_self-managed
- #neurodiversity
